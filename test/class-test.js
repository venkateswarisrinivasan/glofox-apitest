let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;
let moment = require('moment');
let constants = require('./util/constants.js');
let url = constants.url;
let dateUtil = require('./util/date-util');
let formatDateToISOFormat = dateUtil.formatDateToISOFormat;

describe('/class', () => {
    chai.use(chaiHttp);

    it('should create class successfully', function (done) {
        let newClass = {
            className: "Yoga",
            startDate: formatDateToISOFormat(Date.now()),
            endDate: formatDateToISOFormat(Date.now() + 10),
            capacity: 20
        };
        chai.request(url)
            .post('/class')
            .send(newClass)
            .end((err, res) => {
                expect(res.status).to.eq(201);
                let createdClass = res.body;
                expect(createdClass.id).not.null;
                expect(createdClass.className).to.eq(newClass.className);
                expect(createdClass.capacity).to.eq(newClass.capacity);
                expect(formatDateToISOFormat(createdClass.startDate)).to.eq(formatDateToISOFormat(newClass.startDate));
                expect(formatDateToISOFormat(createdClass.endDate)).to.eq(formatDateToISOFormat(newClass.endDate));
                done();
            });
    });

    it('should fail if it miss any field', function (done) {
        let newClass = {
            className: "Yoga",
            endDate: formatDateToISOFormat(Date.now() + 1)  ,
            capacity: 20
        };
        chai.request(url)
            .post('/class')
            .send(newClass)
            .end((err, res) => {
                expect(res.status).to.eq(400);
                done();
            });
    });
});