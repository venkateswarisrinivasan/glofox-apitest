let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;
let constants = require('./util/constants.js');
let url = constants.url;
let dateUtil = require('./util/date-util');
let formatDateToISOFormat = dateUtil.formatDateToISOFormat;

describe('/booking', () => {
    chai.use(chaiHttp);

    it('should create booking successfully', function (done) {
        let newClass = {
            className: "Yoga",
            startDate: formatDateToISOFormat(Date.now()),
            endDate: formatDateToISOFormat(Date.now() + 10),
            capacity: 20
        };
        chai.request(url)
            .post('/class')
            .send(newClass)
            .end((err, res) => {
                expect(res.status).to.eq(201);

                let createdClass = res.body;
                let memberName = 'memberA';
                let bookingDate = formatDateToISOFormat(createdClass.startDate);
                let classId = createdClass.id;

                chai.request(url)
                    .post('/booking')
                    .type('form')
                    .send('studioClassId=' + classId)
                    .send('memberName=' + memberName)
                    .send('classDate=' + bookingDate)
                    .end((err, res) => {
                        expect(res.status).to.eq(201);
                        let booking = res.body;
                        expect(booking.id).not.to.null;
                        expect(booking.member).to.eq(memberName);
                        expect(formatDateToISOFormat(booking.bookingDate)).to.eq(bookingDate);
                        done();
                    });
            });
    })

    it('should return 400 when booking failed', function (done) {

        let memberName = 'memberA';
        let bookingDate = formatDateToISOFormat(Date.now());
        let classId = "";

        chai.request(url)
            .post('/booking')
            .type('form')
            .send('studioClassId=' + classId)
            .send('memberName=' + memberName)
            .send('classDate=' + bookingDate)
            .end((err, res) => {
                expect(res.status).to.eq(400);
                done();
            });
    })
});