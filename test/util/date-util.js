let moment = require('moment');

let formatDateToISOFormat = (date) => {
    return moment(date).format("YYYY-MM-DD");
};

module.exports = {
    formatDateToISOFormat: formatDateToISOFormat
};