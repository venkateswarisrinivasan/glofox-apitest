let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;
let constants = require('./util/constants.js');
let url = constants.url;

describe('/', () => {
    chai.use(chaiHttp)

    it('should test health endpoint', function (done) {
        chai.request(url)
            .get('/')
            .end((err, res) => {
                expect(res.status).to.eq(200);
                expect(res.text).to.eq("App is healthy");
                done();
            });
    });
});